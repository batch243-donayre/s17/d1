// console.log("Hello World")

// [section] Function

	// Function
	 // Functions in javascript are lines/blocks of codes that tells our device/application to perform acertain task when called or invoke.
	// Functions are mostly created to crea a complicated tasks to run several lines of code in succession
	// They are also used to prevent repeating lines/blocks of codes that perform the same task or function


// Function declaration
	// (function statements) - defines a function with specified parameters.

	/*
		Syntax:
		function functionName(){
			code block (statements)
		}
	*/

	// function keyword - used to define a javascript functions
	// functionName - the function name. Fnction are named to be able to use later in the code.
	// function block ({}) - the statements which comprise the body of the function. This is where the code


	function printName(){
		console.log("My name is John.");
		console.log("My last name is Dela cruz.")
	}

	// Function Invocation
		// The code block and statement inside a function is not immediately executed when the function is defined/declared. The code block and statements inside a function is executed when the function is invoked.
		// It is  common to use the term "call a function" instead of "invoke a function"

		// Let's invoke the function that we declared.
		printName();
		// can be used many times / functions are reusable.
		printName();

//[section] Function Declaration and Function Expression

		//Function Declaration
			// A function cn be created through declaration by uaing keyword function and adding function name.

			// Declared functions are not executed immediately.

			// hoisted
			declaredFunction();

			function declaredFunction(){
				console.log("Hello world from declaredFunction");
			}

		// function expression
			 //a function can also be stored in a variable. This is called function expression.

			 // Anonymous function - function without a name. 

			// variableFunction();
			let	variableFunction = function(){
				console.log("Hello for variableFunction!")
			}
			variableFunction();

			let funcExpression = function funcName(){
				console.log("Hello from funcExpression!")
			}
			funcExpression();


			// you can reassign declared function and function espression to new anonymous function.

			declaredFunction = function(){
				console.log("updated declaredFunction");
			}

			declaredFunction();


			funcExpression = function(){
				console.log("updated funcExpression");
			}

			funcExpression();

			// Function expression using const keywprd
			const constantFunc = function(){
				console.log("Initialized with const!")
			}

			constantFunc();

			/*const constantFunc = function(){
				console.log("Const function cannot be reassigned!")
			}
			constantFunc();*/


// [section] Function Scoping
	/*
		Scope is accessiblity of variable within our program.

		Javascript Variables, it  has 3 types ofscope:
		1. local/blockscope
		2. global scope
		3. function scope
	*/

			{
				let localVar = "Armando Perez";
				console.log(localVar);
			}

			let globarVar = "Mr. Worldwide";
			console.log(globarVar);

			// Function Scope
			// Javascript has function scope: Each function creates a new scope.
			// varaible defines inside a function are not accessible outside the function.

			// function showNames(){
			// 	let functionLet = "Jane";
			// 	const functionConst = "John";

			// 	console.log(functionLet);
			// 	console.log(functionConst);
			// }

			// showNames();

			// The variable, functionLet, functionConst, are function scoped and connot be accessible outside of the function that they were declared.

			/*console.log(functionLet);
			console.log(functionConst);*/

// [section] Nested Functions
	// You can create another function inside a function. This is called nestedfunction. This called a nested function.

		function myNewFunction(){
			let name = "Jane";
			console.log(name);

			function nestedFunction(){
				let nestedName = "John";

				console.log(nestedName);
				console.log(name);
			}
			nestedFunction();
		}

		myNewFunction();

		// Function and Global scoped variables
			// Global Scoped Variable

			let globalName = "Alexandro";

				function myNewFunction2(){
					let nameInside = "Renz";

					console.log(globalName);
					console.log(nameInside);
				}

				myNewFunction2();

// [section] Using Alert
	// alert () allows us to show small window at the the top o our browser page to show information to our users. As opposed to console.log() which only shows on he console. It allows us to shw a short dialiog  or instruction to our users. The page will wait until the user dismiss the dialog.
		alert("Hello World");
		// This will run immediately when page loads.

		// Syntax:
		// alert ("<messageInstring>");

		function showSampleAlert(){
			alert("Hello, user!");
		}
		showSampleAlert();

		// Notes on use of alert()
			// Show only an alert() for short dialogs/messagesto the users.
			// Do not overuse alert() because the program/js has to wait for it todismiss b4 continuing.

//[section] promt()

			// promt allows us to show a small window at the top of thw browser to gather user input.
			let samplePrompt = prompt("Enter your name: ");
			console.log(samplePrompt);
			console.log(typeof samplePrompt);

			/*
				Syntax:
					prompt(<>)
			*/

			let sampleNullPrompt = prompt("Don't Enter anything.");
			console.log(sampleNullPrompt);
			console.log(typeof sampleNullPrompt);

			function printWelcomeMessages(){
				let firstName = prompt("Enter your First Name: ");
				let lastName = prompt("Enter your Last Name: ");

				console.log("Hello," +firstName+ " " +lastName + "!");
			}
			printWelcomeMessages();

//[Section] Function Naming Convention
	//Function names should be definitive of the task it will perform. It usually contains a verb.

		function getCourses(){
			let course = ["Science 101", "Math 101", "English 101"];

			console.log(course);
		}
		getCourses();


		// avoid generic names to avoid within your code/program (sample function get())
		function getName(){
			let name = "Jaime"
			console.log(name);
		}
		getName();

		// Avoid pointless and innappropriate function names.

		function foo(){
			console.log(25%5);
		}
		foo();

		// name your funtion in small caps. Follow camelCasing when naming variables and functions.

		function displayCarInfo(){
			console.log("Brand: Toyota");
			console.log("Type: Sedan");
			console.log("Price: 1, 500, 000");

		}
		displayCarInfo();